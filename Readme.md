# ED - EasyDoc
[Last Updated: 2022-02-27]: #


[![AIMMS 4.82.7.10+](https://img.shields.io/badge/AIMMS-4.82.7.10+-blue)](https://aimms.com/support/downloads/)

**Responsáveis**: Jean Patrick, Gabriela Servidone e Rodrigo Mendonça


## Introdução
---

*EasyDoc* (ED) é uma biblioteca AIMMS desenvolvida internamente pela UniSoma com o objetivo de facilitar a geração de documentação interna para desenvolvedores.

A EasyDoc auxilia na construção de documentações extensas e permite padronização da maneira como a documentação interna do projeto é construída. 

Esperamos que os desenvolvedores consigam manter registros de documentação atualizados enquanto codificam, tornando a manutenção da documentação mais um passo natural durante o estágio de desenvolvimento.

A EasyDoc utiliza os templates fornecidos pelo AIMMS via Sphinx para a sua aparência. 


## Instalação
---

Para iniciar o desenvolvimento com a biblioteca EasyDoc, é necessário adicionar este repositório como submódulo do projeto. Você pode ler mais sobre como fazer isso [neste link](https://discuss.unisoma.com/t/git-submodule-vs-subtree/310).



## Utilização

---

A utilização direta da biblioteca ocorre ao chamar o procedimento ed::Proc_ED_GenerateFullDocumentation informando o título da documentação gerada.

Porém, antes de executar o procedimento, a biblitoeca é utilizada indiretamente por meio da adição de:

* marcações em comentários
* annotations em identificadores

As annotations são adicionadas em ed::Documentar e podem ser:

* index: página inicial, deve ser um Ps
* md: um Ps que contém conteúdo em markdown
* doc: um identificado qualquer (parameter, set, procedure) que deve ser documentado

As marcações são adicionadas aos campos coments como segue:

``` 
@author NomeDoAutor \n

@summary Resumo \n

@remarks Observações gerais

@exmaples Exemplos de utilização/chamada

@see Veja parâmetro/procedimento/página importantes e relacionadas

@todo Lista de itens todo
```

Veja mais em ed::Ps_ED_Marks e ed::Ps_ED_Annotations
## Contribuindo
---

No board de projeto podem ser encontradas as melhorias já mapeadas, além de ser possível adicionar novas sugestões. As sugestões devem incluir o máximo de detalhes possível para facilitar o entendimento do que está sendo sugerido. Escrever a sugestão como uma história pode facilitar no momento da descrição:

	Como ____ (papel), eu preciso ____ (funcionalidade) para ____ (motivo)

Antes de adicionar uma sugestão, é sempre uma boa ideia notificar um responsável.

O processo para novos desenvolvimentos segue as seguintes etapas:

1. Notificação a um responsável da história sendo desenvolvida;
2. Correções/alterações/testes feitos diretamente no seu projeto, em uma *branch* separada partindo da *branch develop* deste repositório;
3. Solicitação de união à *branch develop* por *Pull Request*;
4. Aprovação do *Pull Request* por um responsável.